require 'spec_helper'

describe JobOffer do
  describe 'valid?' do
    it 'should be valid when experience is a whole positive number' do
      job_offer = described_class.new(title: 'a title', experience: '7')
      expect(job_offer.experience).to eq('7')
      expect(job_offer).to be_valid
    end

    it 'should be valid when experience is cero' do
      job_offer = described_class.new(title: 'a title', experience: '0')
      expect(job_offer.experience).to eq('0')
      expect(job_offer).to be_valid
    end
  end
end
