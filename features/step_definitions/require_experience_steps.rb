When('I create a new offer with {string} as experience') do |experience|
  @experience = experience
  visit '/job_offers/new'
  fill_in('job_offer_form[title]', with: 'a title')
  fill_in('job_offer_form[experience]', with: experience)
  click_button('Create')
end
