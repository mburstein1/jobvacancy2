Feature: RequireExperience
    In order to requiere RequireExperience
    I want to add the require years of experience in a job offer

Background:
    Given I am logged in as job offerer

Scenario: Sucessful Add Years of Needed Experience
    When I create a new offer with "1" as experience
    Then I should see a offer created confirmation message
    And I should see "1" in my offers list 
